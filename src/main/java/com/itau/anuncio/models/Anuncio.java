package com.itau.anuncio.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Anuncio {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAnuncio;
	
	private int idUsuario;
	private String titulo;
	
	public int getIdAnuncio() {
		return idAnuncio;
	}
	public void setIdAnuncio(int idAnuncio) {
		this.idAnuncio = idAnuncio;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Date getDataExpiracao() {
		return dataExpiracao;
	}
	public void setDataExpiracao(Date dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}
	public boolean isStatusAnuncio() {
		return statusAnuncio;
	}
	public void setStatusAnuncio(boolean statusAnuncio) {
		this.statusAnuncio = statusAnuncio;
	}
	private Date dataExpiracao;
	private boolean statusAnuncio;

}
