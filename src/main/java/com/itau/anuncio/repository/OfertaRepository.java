package com.itau.anuncio.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.anuncio.models.Oferta;

public interface OfertaRepository extends CrudRepository<Oferta, Integer> {

}
