package com.itau.anuncio.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.itau.anuncio.models.Anuncio;


public interface AnuncioRepository extends CrudRepository<Anuncio, Integer>  {
	
	Set<Anuncio> findByIdUsuario(int IdUsuario);

}
