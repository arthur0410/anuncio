package com.itau.anuncio.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.anuncio.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
	
	Usuario findById(int id);
	Optional<Usuario> findByEmail(String email);
}
