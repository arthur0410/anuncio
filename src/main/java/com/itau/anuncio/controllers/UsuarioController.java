package com.itau.anuncio.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.anuncio.autenticacao.PasswordService;
import com.itau.anuncio.autenticacao.TokenService;
import com.itau.anuncio.models.Usuario;
import com.itau.anuncio.repository.UsuarioRepository;

@Controller
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	TokenService tokenService;
	
	@RequestMapping(path="/cadastrar", method=RequestMethod.POST)
	@ResponseBody
	public Usuario inserirUsuario(@RequestBody Usuario usuario) {
		String hash = passwordService.encode(usuario.getSenha());
		usuario.setSenha(hash);
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(path="login", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> logar(@RequestBody Usuario usuario) {
		
		Optional<Usuario> usuarioBanco = usuarioRepository.findByEmail(usuario.getEmail());
		
		if(!usuarioBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		if(passwordService.verificar(usuario.getSenha(), usuarioBanco.get().getSenha())) {
			
			String token = tokenService.gerar(usuarioBanco.get().getId());
			HttpHeaders header = new HttpHeaders();
			header.add("Authorization", token);
			
			return new ResponseEntity<Usuario>(usuarioBanco.get(), header, HttpStatus.OK);
			
		} else {
			return ResponseEntity.badRequest().build();
		}
		
	}
	
	@RequestMapping(path="/usuario", method=RequestMethod.GET)
	@ResponseBody
	public Usuario getUsuarioByEmail(String email) {
		
		return usuarioRepository.findByEmail(email).get();
	}
	
	@RequestMapping(path="/usuario/id", method=RequestMethod.GET)
	@ResponseBody
	public Usuario getUsuarioById(int id) {
		return usuarioRepository.findById(id);
	}

}
