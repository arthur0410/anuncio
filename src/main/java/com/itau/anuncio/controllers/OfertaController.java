package com.itau.anuncio.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.anuncio.models.Oferta;
import com.itau.anuncio.repository.OfertaRepository;

@Controller
public class OfertaController {
	
	@Autowired
	OfertaRepository ofertaRepository;
	
	@RequestMapping(path="/oferta", method=RequestMethod.POST)
	@ResponseBody
	public Oferta inserirAnuncio(@RequestBody Oferta oferta) {
		return ofertaRepository.save(oferta);
	}
	
	@RequestMapping(path="/oferta/varios", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Oferta> getVariosAnuncios() {
	return ofertaRepository.findAll();
	}

}
