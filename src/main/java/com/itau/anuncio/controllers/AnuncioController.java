package com.itau.anuncio.controllers;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.anuncio.autenticacao.TokenService;
import com.itau.anuncio.models.Anuncio;
import com.itau.anuncio.models.Usuario;
import com.itau.anuncio.repository.AnuncioRepository;
import com.itau.anuncio.repository.UsuarioRepository;

@Controller
public class AnuncioController {
	
	@Autowired
	AnuncioRepository anuncioRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	TokenService tokenService;
	
	
	@RequestMapping(path="/anuncio/{id}", method=RequestMethod.POST)
	@ResponseBody
	public Anuncio inserirAnuncio(@PathVariable(value="id") int id,@RequestBody Anuncio anuncio) {
		Usuario usuario = new Usuario();
		usuario = usuarioRepository.findById(id);
		if(usuario != null ) {
			anuncio.setIdUsuario(id);
			return anuncioRepository.save(anuncio);
		}
		return null;
	}
	
	@RequestMapping(path="/anuncio", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Anuncio> consultarAnuncios(HttpServletRequest request) {
		
		System.out.println(request.getHeader("Authorization"));
		String token = request.getHeader("Authorization").replace("Bearer ", "");
		
		int idUsuario = tokenService.verificar(token);
		
		Set<Anuncio> anuncios = new HashSet<>(anuncioRepository.findByIdUsuario(idUsuario));
		
		return anuncios;
		
	}
	
	@RequestMapping(path="/anuncio/varios", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Anuncio> getVariosAnuncios() {
	return anuncioRepository.findAll();
	}

}
